package SecurityLightController;

import SecurityLightController.LightControllerStateMachineObserverInterface.LightState;
import UI.SecurityLampSimulatedUI;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.States;
import org.jmock.internal.State;
import org.junit.Before;
import org.junit.Test;
import SecurityLightController.LightControllerCommandInterface.CommandActionEnum;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author petersjt, chiassond
 *
 * This series of tests uses all-transition coverage.
 * All-action coverage was not possible because some actions are within substates, where they are inacessible.
 * All-states coverage was not used, because the transition between states were the most likely to fail--and so the most necessary to test.
 * All-path coverage was considered redundant, as no transition depends on previous paths.
 *
 * reaching all-transition coverage was not too challenging, as it simply meant making one test per each arrow on the state diagram.
 */
public class LightControllerStateMachineTest {

    //the state machine context object is used for...stuff or something
    private Mockery stateMachineContext;

    private LightControllerCommandInterface lightController;

    private LightControllerStateMachine stateMachine;

    //the state machine's observer
    private LightControllerStateMachineObserverInterface obs;

    private LightDeviceInterface light;

    private Mockery lightContext;

    private LightTimerInterface timer;

    private Mockery timerContext;

    private Mockery observerContext;

    @Before
    public void setUp() throws Exception{
        stateMachineContext = new Mockery();
        lightContext = new Mockery();
        timerContext = new Mockery();
        observerContext = new Mockery();

        obs = observerContext.mock(LightControllerStateMachineObserverInterface.class);

        lightController = stateMachineContext.mock(LightControllerCommandInterface.class);

        light = lightContext.mock(LightDeviceInterface.class);

        timer = timerContext.mock(LightTimerInterface.class);

        stateMachine = new LightControllerStateMachine();
        stateMachine.setLight(light);
        stateMachine.setTmr(timer);
        stateMachine.subscribe(obs);
    }

    @Test
    public void testFromOffDayToFull() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_OFF_DAYLIGHT);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOnFullBrightness();
        }});

        stateMachineContext.checking(new Expectations(){{
            oneOf(obs).updateLightState(LightState.LAMP_ON_FULL_BRIGHTNESS);
        }});

        stateMachine.signalAction(CommandActionEnum.MANUAL_SWITCH_ON);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        assertEquals(LightState.LAMP_ON_FULL_BRIGHTNESS, state);
    }

    @Test
    public void testFromOffDayToOffNight() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_OFF_DAYLIGHT);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOff();
        }});

        stateMachine.signalAction(CommandActionEnum.LIGHT_SENSOR_DARKENED);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        assertEquals(LightState.LAMP_OFF_NIGHTIME, state);
    }

    @Test
    public void testFromOffNightToOnNight() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_OFF_NIGHTIME);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOnNightimeBrightness();
        }});

        stateMachine.signalAction(CommandActionEnum.MANUAL_SWITCH_ON);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        assertEquals(LightState.LAMP_ON_NIGHTIME_BRIGHTNESS, state);
    }

    @Test
    public void testFromOffNightToIntrusion() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_OFF_NIGHTIME);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOnFullBrightness();
        }});

        timerContext.checking(new Expectations(){{
            oneOf(timer).startTimer(1);
        }});

        stateMachineContext.checking(new Expectations(){{
            oneOf(obs).updateLightState(LightState.INTRUSION_DETECTED);
        }});

        stateMachine.signalAction(CommandActionEnum.SECURITY_ALARM_TRIPPED);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        timerContext.assertIsSatisfied();
        assertEquals(LightState.INTRUSION_DETECTED, state);
    }

    @Test
    public void testFromMotionDetectedToIntrusion() throws Exception{

        stateMachine.setCurrentState(LightState.MOTION_DETECTED);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOnFullBrightness();
        }});

        timerContext.checking(new Expectations(){{
            oneOf(timer).startTimer(1);
        }});

        stateMachineContext.checking(new Expectations(){{
            oneOf(obs).updateLightState(LightState.INTRUSION_DETECTED);
        }});

        stateMachine.signalAction(CommandActionEnum.SECURITY_ALARM_TRIPPED);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        timerContext.assertIsSatisfied();

        assertEquals(LightState.INTRUSION_DETECTED, state);
        assertEquals(LightState.INTRUSION_DETECTED, state);
    }

    @Test
    public void testFromOffNightToMotionDetected() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_OFF_NIGHTIME);

        lightContext.checking(new Expectations(){{
            //one for initial state, one for initial substate
            oneOf(light).turnLightOnFullBrightness();
            oneOf(light).turnLightOnFullBrightness();
        }});

        timerContext.checking(new Expectations(){{
            oneOf(timer).startTimer(5);
            oneOf(timer).startTimer(1);
        }});

        stateMachine.signalAction(CommandActionEnum.MOTION_DETECTED);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        timerContext.assertIsSatisfied();

        assertEquals(LightState.MOTION_DETECTED, state);

    }

    @Test
    public void testTimerFromOnToOff() throws Exception{

        stateMachine.setCurrentState(LightState.INTRUSION_DETECTED);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOff();
        }});

        timerContext.checking(new Expectations(){{
            oneOf(timer).startTimer(1);
        }});

        stateMachine.signalAction(CommandActionEnum.LAMP_TIMER_EXPIRED);
        LightState state = stateMachine.getCurrentState();

        assertEquals(LightState.INTRUSION_DETECTED, state);
        lightContext.assertIsSatisfied();
    }

    @Test
    public void testTimerFromOffToOn() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_OFF_NIGHTIME);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOnFullBrightness();
        }});

        timerContext.checking(new Expectations(){{
            oneOf(timer).startTimer(1);
        }});

        stateMachine.signalAction(CommandActionEnum.LAMP_TIMER_EXPIRED);
        LightState state = stateMachine.getCurrentState();

        timerContext.assertIsSatisfied();
        lightContext.assertIsSatisfied();
        assertEquals(LightState.INTRUSION_DETECTED, state);
    }

    @Test
    public void testFromOnNightToOffNight() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_ON_NIGHTIME_BRIGHTNESS);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOff();
        }});

        stateMachineContext.checking(new Expectations(){{
            oneOf(obs).updateLightState(LightState.LAMP_OFF_NIGHTIME);
        }});

        stateMachine.signalAction(CommandActionEnum.MANUAL_SWITCH_OFF);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        assertEquals(state, LightState.LAMP_OFF_NIGHTIME);
        lightContext.assertIsSatisfied();
        assertEquals(LightState.LAMP_OFF_NIGHTIME, state);
    }


    @Test
    public void testFromFullToOffDay() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_ON_FULL_BRIGHTNESS);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOff();
        }});

        stateMachine.signalAction(CommandActionEnum.MANUAL_SWITCH_OFF);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        assertEquals(LightState.LAMP_OFF_DAYLIGHT, state);
    }

    @Test
    public void testFromIntrusionToOffNight() throws Exception{

        stateMachine.setCurrentState(LightState.INTRUSION_DETECTED);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOff();
        }});

        timerContext.checking(new Expectations(){{
            oneOf(timer).startTimer(1);
        }});

        stateMachine.signalAction(CommandActionEnum.ALARM_CLEARED);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        assertEquals(LightState.LAMP_OFF_NIGHTIME, state);
    }

    @Test
    public void testFromOffNightToOffDay() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_OFF_NIGHTIME);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOff();
        }});

        stateMachine.signalAction(CommandActionEnum.LIGHT_SENSOR_LIGHTENED);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        assertEquals(LightState.LAMP_OFF_DAYLIGHT, state);
    }

    @Test
    public void testFromMotionDetectedToOffNight() throws Exception{

        stateMachine.setCurrentState(LightState.MOTION_DETECTED);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOff();
        }});

        timerContext.checking(new Expectations(){{
            oneOf(timer).startTimer(1);
        }});

        stateMachine.signalAction(CommandActionEnum.LAMP_TIMER_EXPIRED);
        LightState state = stateMachine.getCurrentState();

        timerContext.assertIsSatisfied();
        lightContext.assertIsSatisfied();
        assertEquals(LightState.LAMP_OFF_NIGHTIME, state);
    }

    @Test
    public void testFromOnNightToFull() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_ON_NIGHTIME_BRIGHTNESS);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOnFullBrightness();
        }});

        stateMachine.signalAction(CommandActionEnum.LIGHT_SENSOR_LIGHTENED);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        assertEquals(LightState.LAMP_ON_FULL_BRIGHTNESS, state);
    }

    @Test
    public void testFromFullToOnNight() throws Exception{

        stateMachine.setCurrentState(LightState.LAMP_ON_FULL_BRIGHTNESS);

        lightContext.checking(new Expectations(){{
            oneOf(light).turnLightOnNightimeBrightness();
        }});

        stateMachine.signalAction(CommandActionEnum.LIGHT_SENSOR_DARKENED);
        LightState state = stateMachine.getCurrentState();

        lightContext.assertIsSatisfied();
        assertEquals(state, LightState.LAMP_ON_NIGHTIME_BRIGHTNESS);
    }

}