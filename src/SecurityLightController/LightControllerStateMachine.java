package SecurityLightController;

import java.util.ArrayList;
import java.util.List;

import SecurityLightController.LightControllerStateMachineObserverInterface.LightState;

/**
 * This class implements a state machine that can be used to control a security
 * light.
 *
 * @author schilling
 *
 */
public class LightControllerStateMachine implements
		LightControllerCommandInterface {

	private final class TransitionTableEntry {
		LightState presentState;
		CommandActionEnum event;
		LightState destinationState;

		private TransitionTableEntry(LightState presentState,
									 CommandActionEnum event, LightState destinationState) {
			super();
			this.presentState = presentState;
			this.event = event;
			this.destinationState = destinationState;
		}

	}

	/**
	 * This variable holds a list of observers. Whenever a state changes, the
	 * observers are updated with the state change.
	 */
	private List<LightControllerStateMachineObserverInterface> observers = new ArrayList<LightControllerStateMachineObserverInterface>();

	/**
	 * This variable holds the current state for the state machine.
	 */
	private LightState currentState;

	/**
	 * This variable holds the light which is to be controlled by this state
	 * machine.
	 */
	private LightDeviceInterface light;

	/**
	 * This variable is the instance of the timer that is to be used for timed
	 * activities.
	 */
	private LightTimerInterface tmr;

	/**
	 * This value is used for keeping track of the Intrusion detected substates.
	 */
	private final static int INTRUSION_DETECTED_LAMP_ON = 1;

	/**
	 * This value is used for keeping track of the Intrusion detected substates.
	 */
	private final static int INTRUSION_DETECTED_LAMP_OFF = 2;

	/**
	 * This variable holds the substate for the intrusion detected state.
	 */
	private int intrusionDetectionStateVariable;

	/**
	 * This is the default constructor, which will instantiate a new instance of
	 * this class.
	 */
	public LightControllerStateMachine() {
		currentState = LightState.LAMP_OFF_DAYLIGHT;
	}

	/**
	 * This method will set the light that is to be controlled by this state
	 * machine.
	 *
	 * @param light
	 *            This is the instance of the light that is to be directly
	 *            controlled.
	 */
	public void setLight(LightDeviceInterface light) {
		this.light = light;
	}

	/**
	 * This method will set an instance of the timer that is to be used with
	 * this class.
	 *
	 * @param tmr
	 *            This is the timer instance.
	 */
	public void setTmr(LightTimerInterface tmr) {
		this.tmr = tmr;
	}

	/**
	 * This method will be invoked to update the observers
	 *
	 * @param state
	 *            This is the new state.
	 */
	private void updateState(LightState state) {
		for (LightControllerStateMachineObserverInterface obs : this.observers) {
//			obs.updateLightState(state);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeSecurityLightController.LightControllerCommandInterface#subscribe(
	 * SecurityLightController.LightControllerStateMachineObserverInterface)
	 */
	public void subscribe(LightControllerStateMachineObserverInterface obs) {
		this.observers.add(obs);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeSecurityLightController.LightControllerCommandInterface#unsubscribe(
	 * SecurityLightController.LightControllerStateMachineObserverInterface)
	 */
	public void unsubscribe(LightControllerStateMachineObserverInterface obs) {
		this.observers.remove(obs);

	}

	/**
	 * This method will process exit conditions based upon leaving a state.
	 *
	 * @param presentState
	 *            This is the present state. It will be used to determine which
	 *            exit actions to invoke.
	 */
	private void handleExitConditions(LightState presentState) {
		// Based on the present state, switch and invoke the exit actions.
		switch (presentState) {
			case LAMP_OFF_DAYLIGHT:
				// No exit actions exist.
				break;

			case LAMP_ON_FULL_BRIGHTNESS:
				// No exit actions exist.
				break;

			case LAMP_OFF_NIGHTIME:
				// No exit actions exist.
				break;

			case LAMP_ON_NIGHTIME_BRIGHTNESS:
				// No exit actions exist.
				break;

			case MOTION_DETECTED:
				break;


			case INTRUSION_DETECTED:

				break;

			default:
				break;
		}
	}

	/**
	 * This method will process entry conditions based upon entering a state.
	 *
	 * @param presentState
	 *            This is the present state. It will be used to determine which
	 *            exit actions to invoke.
	 */
	private void handleEntryConditions(LightState presentState) {
		switch (presentState) {
			case LAMP_OFF_DAYLIGHT:
				light.turnLightOff();
				break;

			case LAMP_ON_FULL_BRIGHTNESS:
				light.turnLightOnFullBrightness();
				break;

			case LAMP_OFF_NIGHTIME:
				light.turnLightOff();
				break;

			case LAMP_ON_NIGHTIME_BRIGHTNESS:
				light.turnLightOnNightimeBrightness();
				break;

			case MOTION_DETECTED:
				tmr.startTimer(5);
				light.turnLightOnFullBrightness();


			case INTRUSION_DETECTED:
				// Initialize the substate appropriately.
				intrusionDetectionStateVariable = INTRUSION_DETECTED_LAMP_ON;

				// Start the timer.
				tmr.startTimer(1);

				// Adjust the light setting.
				light.turnLightOnFullBrightness();
				break;

			default:
				break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * SecurityLightController.LightControllerCommandInterface#signalAction(int)
	 */
	public void signalAction(CommandActionEnum request) {

		// This is a local variable which has the present state of the system.
		LightState presentState = this.currentState;

		// This is the variable which holds the destination state for the
		// system.
		// We will determine it through the case statement.
		LightState destinationState = presentState;
		// This variable will indicate if a state change is necessary.
		boolean stateChange = false;

		// This switch state will determine the destination state that we need
		// to enter. Based on that, make the correct state changes.
		switch (presentState) {
			case LAMP_OFF_DAYLIGHT:
				if (request == CommandActionEnum.MANUAL_SWITCH_ON) {
					destinationState = LightState.LAMP_ON_FULL_BRIGHTNESS;
					stateChange = true;
				} else if (request == CommandActionEnum.LIGHT_SENSOR_DARKENED) {
					destinationState = LightState.LAMP_OFF_NIGHTIME;
					stateChange = true;
				} else {
					// No state change here.
					stateChange = false;
				}
				break;

			// In the following, we handle transitions where the starting state is
			// lamp on full brightness.
			case LAMP_ON_FULL_BRIGHTNESS:
				if (request == CommandActionEnum.MANUAL_SWITCH_OFF) {
					destinationState = LightState.LAMP_OFF_DAYLIGHT;
					stateChange = true;
				} else if (request == CommandActionEnum.LIGHT_SENSOR_DARKENED) {
					destinationState = LightState.LAMP_ON_NIGHTIME_BRIGHTNESS;
					stateChange = true;
				} else {
					// No state change here.
					stateChange = false;
				}
				break;

			case LAMP_OFF_NIGHTIME:
			case LAMP_ON_NIGHTIME_BRIGHTNESS:

				// The following is a table of state transitions. In essence, the
				// first element of each row is a current state. This is followed by
				// an action that occurs. The third entry is the destination state.
				// When an action occurs, we will try to find a starting state and
				// an event which match. If so, the transition to the outgoing state
				// will occur.
				// This method, using state tables, is very common in embedded
				// systems. But, it also relies on very careful analysis of the
				// state chart when designing test cases. However, if a state chart
				// is designed properly, the state tables can be automatically
				// generated.
				// It is possible to use a more advanced data structure than this,
				// but this is the easiest to understand as a starting point.

				TransitionTableEntry[] lampOnNightimeBrightnessAndLampOffNighttimeBrightnessTransitionTable = {
						new TransitionTableEntry(LightState.LAMP_OFF_NIGHTIME,
								CommandActionEnum.MOTION_DETECTED,
								LightState.MOTION_DETECTED),
						new TransitionTableEntry(LightState.LAMP_OFF_NIGHTIME,
								CommandActionEnum.MANUAL_SWITCH_ON,
								LightState.LAMP_ON_NIGHTIME_BRIGHTNESS),
						new TransitionTableEntry(LightState.LAMP_OFF_NIGHTIME,
								CommandActionEnum.SECURITY_ALARM_TRIPPED,
								LightState.INTRUSION_DETECTED),
						new TransitionTableEntry(LightState.LAMP_OFF_NIGHTIME,
								CommandActionEnum.LIGHT_SENSOR_LIGHTENED,
								LightState.LAMP_OFF_DAYLIGHT),

						new TransitionTableEntry(
								LightState.LAMP_ON_NIGHTIME_BRIGHTNESS,
								CommandActionEnum.MANUAL_SWITCH_OFF,
								LightState.LAMP_OFF_NIGHTIME),
						new TransitionTableEntry(
								LightState.LAMP_ON_NIGHTIME_BRIGHTNESS,
								CommandActionEnum.LIGHT_SENSOR_LIGHTENED,
								LightState.LAMP_ON_FULL_BRIGHTNESS) };

				// The following loop will step over the code looking for a match.
				// You could use some form of a hashmap for this in a more advanced
				// implementation if you wanted to. Does this seem any easier to
				// test than the other parts?
				int index = 0;
				while ((stateChange == false)
						&& (index < lampOnNightimeBrightnessAndLampOffNighttimeBrightnessTransitionTable.length)) {
					if ((presentState == lampOnNightimeBrightnessAndLampOffNighttimeBrightnessTransitionTable[index].presentState)
							&& (request == lampOnNightimeBrightnessAndLampOffNighttimeBrightnessTransitionTable[index].event)) {
						stateChange = true;
						destinationState = lampOnNightimeBrightnessAndLampOffNighttimeBrightnessTransitionTable[index].destinationState;

					} else {
						index++;
					}
				}

				break;

			// Here is where we are handling items where motion has been previously
			// detected.

			case MOTION_DETECTED:
				if (request == CommandActionEnum.SECURITY_ALARM_TRIPPED) {
					destinationState = LightState.INTRUSION_DETECTED;
					stateChange = true;
				} else if (request == CommandActionEnum.LAMP_TIMER_EXPIRED) {
					destinationState = LightState.LAMP_OFF_NIGHTIME;
					stateChange = true;
				} else {
					// No state change here.
					stateChange = false;
				}


				// Here is where intrusion detection is handled.
				// This one is a bit bigger from a detail standpoint because there are
				// multiple sub-states in this state machine.
			case INTRUSION_DETECTED:
				if (request == CommandActionEnum.ALARM_CLEARED) {
					destinationState = LightState.LAMP_OFF_NIGHTIME;
					stateChange = true;
				} else if (request == CommandActionEnum.LAMP_TIMER_EXPIRED) {
					if (intrusionDetectionStateVariable == INTRUSION_DETECTED_LAMP_ON) {
						// Toggle the variable to change the substate.
						intrusionDetectionStateVariable = INTRUSION_DETECTED_LAMP_OFF;

						// Start the timer.
						tmr.startTimer(1);

						// Adjust the light setting.
						light.turnLightOff();
					} else if (intrusionDetectionStateVariable == INTRUSION_DETECTED_LAMP_OFF) {
						// Toggle the variable to change the substate.
						intrusionDetectionStateVariable = INTRUSION_DETECTED_LAMP_ON;

						// Start the timer.
						tmr.startTimer(1);

						// Adjust the light setting.
						light.turnLightOnFullBrightness();
					} else {
						// Something went wrong. Start back with the initial
						// conditions.
						// Toggle the variable to change the substate.
						intrusionDetectionStateVariable = INTRUSION_DETECTED_LAMP_ON;

						// Start the timer.
						tmr.startTimer(1);

						// Adjust the light setting.
						light.turnLightOff();

					}
					stateChange = false;
				}

			default:
				break;
		}

		// Now determine if an actual state change has occurred. If so, invoke
		// the exit action on the given state as well as the entry action for
		// the new state.
		if (stateChange == true) {
			// Invoke exit actions.
			handleExitConditions(presentState);

			// Invoke entry actions.
			handleEntryConditions(destinationState);

			// Update the state variable.
			this.currentState = destinationState;

			// Update the observers.
			updateState(destinationState);
		}
	}

	/**
	 * This method will return the current state of the Light controller. It is
	 * only intended to be used for testing. Thus, the protected attribute.
	 *
	 * @return This will return the current state of the light controller.
	 */
	LightState getCurrentState() {
		return currentState;
	}

	/**
	 * This method will set the current state of the light controller. It is
	 * intended to only be used for testing, as it may or may not properly
	 * invoke any entry or exit conditions. Note that this method specifically
	 * has limited scope so it only can be accessed from the class or package to
	 * prevent problems.
	 *
	 * @param currentState
	 *            This is the desired state for the light controller.
	 */
	void setCurrentState(LightState currentState) {
		this.currentState = currentState;
	}
}
